package gymcon.pl.gymcon;

import com.kontakt.sdk.android.common.profile.IBeaconDevice;

import java.util.UUID;

/**
 * Created by piotrek on 11.06.16.
 */
public class BeaconInfo {
    private String name;
    private UUID proximityId;
    private int majorId;
    private int minorId;
    private double distance;

    public BeaconInfo(String name, UUID proximityId, int majorId, int minorId) {
        this.name = name;
        this.proximityId = proximityId;
        this.majorId = majorId;
        this.minorId = minorId;
    }

    public BeaconInfo(IBeaconDevice ibeacon) {
        this(ibeacon.getUniqueId(), ibeacon.getProximityUUID(), ibeacon.getMajor(), ibeacon.getMinor());
        setDistance(ibeacon.getDistance());
    }

    public UUID getProximityId() {
        return proximityId;
    }

    public void setProximityId(UUID proximityId) {
        this.proximityId = proximityId;
    }

    public int getMajorId() {
        return majorId;
    }

    public void setMajorId(int majorId) {
        this.majorId = majorId;
    }

    public int getMinorId() {
        return minorId;
    }

    public void setMinorId(int minorId) {
        this.minorId = minorId;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BeaconInfo that = (BeaconInfo) o;

        if (majorId != that.majorId) return false;
        if (minorId != that.minorId) return false;
        return proximityId != null ? proximityId.equals(that.proximityId) : that.proximityId == null;

    }

    @Override
    public int hashCode() {
        int result = proximityId != null ? proximityId.hashCode() : 0;
        result = 31 * result + majorId;
        result = 31 * result + minorId;
        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
