package gymcon.pl.gymcon;

import android.os.AsyncTask;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class TrainingListRequestService extends AsyncTask<String, Void, String> {
    private OkHttpClient client = new OkHttpClient();
    private MapActivity activity;
    private String planId;

    public TrainingListRequestService(MapActivity activity, String planId) {
        this.activity = activity;
        this.planId = planId;
    }

    @Override
    protected String doInBackground(String... params) {
        Request request = new Request.Builder()
                .url(params[0])
                .build();

        try {
            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    @Override
    protected void onPostExecute(String json) {
        List<TrainingPlan> trainingPlans = new TrainingPlanService().parse(json);
        for (TrainingPlan trainingPlan : trainingPlans) {
            if (planId.equals(trainingPlan.getPlanId())) {
                Training training = new Training(trainingPlan);
                activity.setTraining(training);
                break;
            }
        }
    }
}
