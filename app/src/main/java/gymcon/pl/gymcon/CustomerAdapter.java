package gymcon.pl.gymcon;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomerAdapter extends ArrayAdapter<TrainingStep> {

    private final int currentStep;



    public CustomerAdapter(Context context, int resource, List<TrainingStep> items, int currentStep) {
        super(context, resource, items);
        this.currentStep = currentStep;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.drawer_item, null);
        }

        TrainingStep p = getItem(position);

        if (p != null) {
            TextView tt1 = (TextView) v.findViewById(R.id.step_name);
            TextView tt2 = (TextView) v.findViewById(R.id.step_weight);
            TextView tt3 = (TextView) v.findViewById(R.id.step_reps);

            TextView tt12 = (TextView) v.findViewById(R.id.step_name2);
            TextView tt22 = (TextView) v.findViewById(R.id.step_weight2);
            TextView tt32 = (TextView) v.findViewById(R.id.step_reps2);
            ImageView statusView = (ImageView) v.findViewById(R.id.step_status);

            if (currentStep  == p.getOrder()){
                statusView.setImageResource(R.drawable.circle_progress);
            } else if (currentStep > p.getOrder()){
                statusView.setImageResource(R.drawable.circle_tick);
            } else {
                statusView.setImageResource(R.drawable.circle_find);
            }

            if (position % 2 == 0){

                if (tt1 != null) {
                    tt1.setText(p.getMachine().getName());
                }

                if (tt2 != null) {
                    tt2.setText(String.valueOf(p.getWeightKg())+ "KG");
                }

                if (tt3 != null) {
                    tt3.setText("REPS: " + p.getRepeats());
                }
            } else {

                if (tt12 != null) {
                    tt12.setText(p.getMachine().getName());
                }

                if (tt22 != null) {
                    tt22.setText(String.valueOf(p.getWeightKg())+ "KG");
                }

                if (tt32 != null) {
                    tt32.setText("REPS: " + p.getRepeats());
                }
            }

        }

        return v;
    }

}