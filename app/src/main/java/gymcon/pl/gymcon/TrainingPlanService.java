package gymcon.pl.gymcon;

import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

public class TrainingPlanService {
    private Gson gson = new Gson();

    public List<TrainingPlan> parse(String json) {
        TrainingPlan[] trainingPlans = gson.fromJson(json, TrainingPlan[].class);
        return Arrays.asList(trainingPlans);
    }
}
