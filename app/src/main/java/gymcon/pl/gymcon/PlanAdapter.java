package gymcon.pl.gymcon;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.List;

public class PlanAdapter extends ArrayAdapter<TrainingPlan> {


    public PlanAdapter(Context context, int resource, List<TrainingPlan> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.plan_item, null);
        }

        TrainingPlan item = getItem(position);

        ImageView planImage = (ImageView) v.findViewById(R.id.plan_image);

        switch (item.getPlanId()) {
            case "1":
                planImage.setImageResource(R.drawable.training1);
                break;
            case "2":
                planImage.setImageResource(R.drawable.training2);
                break;
            case "3":
                planImage.setImageResource(R.drawable.training3);
                break;
            case "4":
                planImage.setImageResource(R.drawable.training4);
                break;
        }
        return v;
    }


}
