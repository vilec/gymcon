package gymcon.pl.gymcon;

import com.google.gson.Gson;

public class MachineInfoService {
    private Gson gson = new Gson();

    public MachineInfo parse(String json) {
        return gson.fromJson(json, MachineInfo.class);
    }
}
