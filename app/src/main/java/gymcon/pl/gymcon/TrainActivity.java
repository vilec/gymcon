package gymcon.pl.gymcon;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.kontakt.sdk.android.ble.connection.OnServiceReadyListener;
import com.kontakt.sdk.android.ble.manager.ProximityManager;
import com.kontakt.sdk.android.ble.manager.ProximityManagerContract;
import com.kontakt.sdk.android.ble.manager.listeners.EddystoneListener;
import com.kontakt.sdk.android.ble.manager.listeners.IBeaconListener;
import com.kontakt.sdk.android.ble.manager.listeners.simple.SimpleEddystoneListener;
import com.kontakt.sdk.android.ble.manager.listeners.simple.SimpleIBeaconListener;
import com.kontakt.sdk.android.common.KontaktSDK;
import com.kontakt.sdk.android.common.profile.IBeaconDevice;
import com.kontakt.sdk.android.common.profile.IBeaconRegion;
import com.kontakt.sdk.android.common.profile.IEddystoneDevice;
import com.kontakt.sdk.android.common.profile.IEddystoneNamespace;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TrainActivity extends Activity {

    private ProximityManagerContract proximityManager;
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private int stepId;

    private Map<String, String> machineBeaconsToMovingProximityId = new HashMap<>();
    private Map<String, String> beaconsToLastProximityId = new HashMap<>();

    public TrainActivity() {
        machineBeaconsToMovingProximityId.put("AmwF", "4b6f6e74-616b-742e-696f-4d6f74696f6e");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.train);

        KontaktSDK.initialize(this);

        proximityManager = new ProximityManager(this);
        proximityManager.configuration().deviceUpdateCallbackInterval(10500);
        proximityManager.setIBeaconListener(createIBeaconListener());

        Intent intent = getIntent();
        String planId = intent.getStringExtra("planId");
        stepId = intent.getIntExtra("stepId", 1);

        imageLoader.init(ImageLoaderConfiguration.createDefault(getBaseContext()));

        new HttpRequestService(imageLoader, this, planId, stepId).execute("http://" + Constants.SERVER_IP + "/GymConServer/api/TrainingPlans/");

    }


    public void setTrainingPlan(TrainingPlan trainingPlan) {

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ListView mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerList.setAdapter(new CustomerAdapter(this, R.layout.drawer_item, trainingPlan.getTrainingSteps(), stepId));

    }

    private void notifyWear(int reps){
        int notificationId = 001;
        // Build intent for notification content
        Intent viewIntent = new Intent(this, MainActivity.class);
       // viewIntent.putExtra(EXTRA_EVENT_ID, eventId);
        PendingIntent viewPendingIntent =
                PendingIntent.getActivity(this, 0, viewIntent, 0);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setVibrate(new long[] {1000,1000})
                        .setSmallIcon(R.drawable.playstore_icon)
                        .setLargeIcon(BitmapFactory.decodeResource(
                                getResources(), R.drawable.playstore_icon))
                        .setContentTitle("gymy")
                        .setContentText("Reps left: " + reps)
                        .setContentIntent(viewPendingIntent);

        // Get an instance of the NotificationManager service
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);

        // Build the notification and issues it with notification manager.
        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    private IBeaconListener createIBeaconListener() {
        return new SimpleIBeaconListener() {
            @Override
            public void onIBeaconsUpdated(List<IBeaconDevice> ibeacons, IBeaconRegion region) {
                for (IBeaconDevice ibeacon : ibeacons) {
                    if (machineBeaconsToMovingProximityId.containsKey(ibeacon.getUniqueId())) {

                        Log.i("gymy", "Movement: " + ibeacon.getUniqueId() + " "+ ibeacon.getProximityUUID());

                        String lastProximityUuid = beaconsToLastProximityId.get(ibeacon.getUniqueId());
                        String currentUuid = ibeacon.getProximityUUID().toString();
                        beaconsToLastProximityId.put(ibeacon.getUniqueId(), currentUuid);

                        String uuidAfterMove = machineBeaconsToMovingProximityId.get(ibeacon.getUniqueId());
                        if (uuidAfterMove.equals(currentUuid) && !currentUuid.equals(lastProximityUuid)) {
                            TextView repsFirstDigit = (TextView) findViewById(R.id.textView5);
                            TextView repsSecondDigit = (TextView) findViewById(R.id.textView4);

                            int reps = Integer.valueOf((String) repsFirstDigit.getText()) * 10;
                            reps += Integer.valueOf((String) repsSecondDigit.getText());
                            reps--;


                            notifyWear(reps);

                            repsFirstDigit.setText(String.valueOf(reps / 10));
                            repsSecondDigit.setText(String.valueOf(reps % 10));

                            if (reps == 0) {
                                Intent resultIntent = new Intent();
                                setResult(Activity.RESULT_OK, resultIntent);
                                finish();
                            }
                        }
                    }
                }
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        startScanning();
    }

    @Override
    protected void onStop() {
        proximityManager.stopScanning();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        proximityManager.disconnect();
        proximityManager = null;
        super.onDestroy();
    }

    private void startScanning() {
        proximityManager.connect(new OnServiceReadyListener() {
            @Override
            public void onServiceReady() {
                proximityManager.startScanning();
            }
        });
    }

}
