package gymcon.pl.gymcon;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.customlbs.library.IndoorsException;
import com.customlbs.library.IndoorsFactory;
import com.customlbs.library.IndoorsLocationListener;
import com.customlbs.library.callbacks.LoadingBuildingStatus;
import com.customlbs.shared.Coordinate;
import com.customlbs.surface.library.IndoorsSurfaceFactory;
import com.customlbs.surface.library.IndoorsSurfaceFragment;
import com.kontakt.sdk.android.ble.connection.OnServiceReadyListener;
import com.kontakt.sdk.android.ble.manager.ProximityManager;
import com.kontakt.sdk.android.ble.manager.ProximityManagerContract;
import com.kontakt.sdk.android.ble.manager.listeners.EddystoneListener;
import com.kontakt.sdk.android.ble.manager.listeners.IBeaconListener;
import com.kontakt.sdk.android.ble.manager.listeners.simple.SimpleEddystoneListener;
import com.kontakt.sdk.android.ble.manager.listeners.simple.SimpleIBeaconListener;
import com.kontakt.sdk.android.common.KontaktSDK;
import com.kontakt.sdk.android.common.profile.IBeaconDevice;
import com.kontakt.sdk.android.common.profile.IBeaconRegion;
import com.kontakt.sdk.android.common.profile.IEddystoneDevice;
import com.kontakt.sdk.android.common.profile.IEddystoneNamespace;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import com.customlbs.library.callbacks.RoutingCallback;
import com.customlbs.library.model.Building;
import com.customlbs.library.model.Zone;


public class MapActivity extends AppCompatActivity implements IndoorsLocationListener {
    public static final int REQUEST_CODE = 1;

    private ProximityManagerContract proximityManager;
    private NearestService nearestService = new NearestService();
    private Training training;
    private AtomicBoolean alreadyOpened = new AtomicBoolean(false);
    private boolean setPath = false;
    private IndoorsSurfaceFragment indoorsFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.map);

        KontaktSDK.initialize(this);

        proximityManager = new ProximityManager(this);
        proximityManager.setIBeaconListener(createIBeaconListener());
        proximityManager.setEddystoneListener(createEddystoneListener());

        Intent intent = getIntent();
        String planId = intent.getStringExtra("planId");

        IndoorsFactory.Builder indoorsBuilder = new IndoorsFactory.Builder();
        IndoorsSurfaceFactory.Builder surfaceBuilder = new IndoorsSurfaceFactory.Builder();

        indoorsBuilder.setContext(this);

        // TODO: replace this with your API-key
        indoorsBuilder.setApiKey("29701eed-461d-49c6-8573-d8f7d8d703d5");

        // TODO: replace 12345 with the id of the building you uploaded to
        // our cloud using the MMT
        indoorsBuilder.setBuildingId((long) 776996197);
        indoorsBuilder.setUserInteractionListener(this);
        surfaceBuilder.setIndoorsBuilder(indoorsBuilder);

        indoorsFragment = surfaceBuilder.build();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.map_frame, indoorsFragment, "indoors");
        transaction.commit();

        new TrainingListRequestService(this, planId).execute("http://" + Constants.SERVER_IP + "/GymConServer/api/TrainingPlans/");

        Log.i("gymy", "Map activity");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case (REQUEST_CODE): {
                if (resultCode == Activity.RESULT_OK) {
                    if (training != null) {
                        training.incrementStep();
                        if (training.isFinished()) {
                            Intent intent = new Intent(MapActivity.this, FinishTrainingActivity.class);
                            startActivity(intent);
                        }
                        alreadyOpened.set(false);
                    }
                }
                break;
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        startScanning();
    }

    @Override
    protected void onStop() {
        proximityManager.stopScanning();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        proximityManager.disconnect();
        proximityManager = null;
        super.onDestroy();
    }

    private void startScanning() {
        proximityManager.connect(new OnServiceReadyListener() {
            @Override
            public void onServiceReady() {
                proximityManager.startScanning();
            }
        });
    }

    public void setTraining(Training training) {
        this.training = training;
    }


    private IBeaconListener createIBeaconListener() {
        return new SimpleIBeaconListener() {

            @Override
            public void onIBeaconsUpdated(List<IBeaconDevice> ibeacons, IBeaconRegion region) {
                BeaconInfo beacon = nearestService.findNearest(ibeacons);

                synchronized (MainActivity.class) {
                    if (!alreadyOpened.get() && training != null && beacon != null && beacon.getName().equals(training.getNextMachineBeacon())) {
                        alreadyOpened.set(true);
                        Log.i("gymy", "Machine located nearby: " + beacon.getName() + ", distance: " + beacon.getDistance());

                        Intent intent = new Intent(MapActivity.this, TrainActivity.class);
                        intent.putExtra("ID", beacon.getName());
                        intent.putExtra("planId", training.getPlanId());
                        intent.putExtra("stepId", training.getStep());
                        startActivityForResult(intent, REQUEST_CODE);
                    }
                }
            }
        };
    }

    private EddystoneListener createEddystoneListener() {
        return new SimpleEddystoneListener() {
            @Override
            public void onEddystoneDiscovered(IEddystoneDevice eddystone, IEddystoneNamespace namespace) {
            }
        };
    }

    @Override
    public void loadingBuilding(LoadingBuildingStatus loadingBuildingStatus) {

    }

    @Override
    public void buildingLoaded(Building building) {
        //Toast.makeText(getApplicationContext(), "Building Loaded", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void leftBuilding(Building building) {

    }

    @Override
    public void positionUpdated(Coordinate coordinate, int i) {
       // Toast.makeText(getApplicationContext(), "Got update " + coordinate.x + " : " + coordinate.y, Toast.LENGTH_SHORT).show();

        if(!setPath){
            setPath = true;
            Coordinate end = new Coordinate(44404, 11000, 0);
            indoorsFragment.getIndoors().getRouteAToB(coordinate, end, new RoutingCallback() {
                @Override
                public void onError(IndoorsException arg0) {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();

                    Log.w("W", "Message", arg0);
                }

                @Override
                public void setRoute(ArrayList<Coordinate> route) {
                  //  Toast.makeText(getApplicationContext(), "Updating path", Toast.LENGTH_SHORT).show();

                    indoorsFragment.getSurfaceState().setRoutingPath(route);
                    // this how to enable route snapping starting 3.8
                    IndoorsFactory.getInstance().enableRouteSnapping(route);
                    indoorsFragment.updateSurface();
                }
            });
        }
    }

    @Override
    public void orientationUpdated(float v) {

    }

    @Override
    public void changedFloor(int i, String s) {

    }

    @Override
    public void enteredZones(List<Zone> list) {

    }

    @Override
    public void buildingLoadingCanceled() {

    }

    @Override
    public void onError(IndoorsException e) {

    }
}
