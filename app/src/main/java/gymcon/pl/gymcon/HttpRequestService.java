package gymcon.pl.gymcon;

import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HttpRequestService extends AsyncTask<String, Void, String> {
    private OkHttpClient client = new OkHttpClient();
    private TrainingPlanService trainingPlanService = new TrainingPlanService();
    private ImageLoader imageLoader;
    private TrainActivity activity;
    private String planId;
    private int stepId;

    public HttpRequestService(ImageLoader imageLoader, TrainActivity trainActivity, String planId, int stepId) {
        this.imageLoader = imageLoader;
        this.activity = trainActivity;
        this.planId = planId;
        this.stepId = stepId;
    }

    @Override
    protected String doInBackground(String... params) {
        Request request = new Request.Builder()
                .url(params[0])
                .build();

        try {
            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    @Override
    protected void onPostExecute(String json) {
        TrainingStep trainingStep = findTrainingStep(json);
        if (trainingStep != null) {
            MachineInfo machineInfo = trainingStep.getMachine();
            if (machineInfo != null) {
                if (machineInfo.getImgUrl() != null) {
                    ImageView image = (ImageView) activity.findViewById(R.id.imageView);
                    imageLoader.displayImage(machineInfo.getImgUrl(), image);
                }

                TextView machineName = (TextView) activity.findViewById(R.id.machineName);
                machineName.setText(machineInfo.getName());
            }

            TextView currentWeight = (TextView) activity.findViewById(R.id.textView6);
            TextView suggestedWeight = (TextView) activity.findViewById(R.id.textView8);
            currentWeight.setText(String.valueOf(trainingStep.getWeightKg()+"KG"));
            suggestedWeight.setText(String.valueOf(trainingStep.getWeightKg()+"KG"));

            TextView repsFirstDigit = (TextView) activity.findViewById(R.id.textView5);
            TextView repsSecondDigit = (TextView) activity.findViewById(R.id.textView4);

            repsFirstDigit.setText(String.valueOf(trainingStep.getRepeats() / 10));
            repsSecondDigit.setText(String.valueOf(trainingStep.getRepeats() % 10));
        }
    }

    private TrainingStep findTrainingStep(String json) {
        List<TrainingPlan> trainingPlans = trainingPlanService.parse(json);
        for (TrainingPlan trainingPlan : trainingPlans) {
            if (planId.equals(trainingPlan.getPlanId())) {
                activity.setTrainingPlan(trainingPlan);
                for (TrainingStep trainingStep : trainingPlan.getTrainingSteps()) {
                    if (stepId == trainingStep.getOrder()) {
                        return trainingStep;
                    }
                }
            }
        }
        return null;
    }
}
