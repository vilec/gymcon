package gymcon.pl.gymcon;

public class TrainingStep {
    private MachineInfo machine;
    private int order;
    private int weightKg;
    private int repeats;
    private int series;

    public MachineInfo getMachine() {
        return machine;
    }

    public void setMachine(MachineInfo machine) {
        this.machine = machine;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getWeightKg() {
        return weightKg;
    }

    public void setWeightKg(int weightKg) {
        this.weightKg = weightKg;
    }

    public int getRepeats() {
        return repeats;
    }

    public void setRepeats(int repeats) {
        this.repeats = repeats;
    }

    public int getSeries() {
        return series;
    }

    public void setSeries(int series) {
        this.series = series;
    }
}
