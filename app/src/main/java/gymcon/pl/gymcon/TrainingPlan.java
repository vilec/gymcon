package gymcon.pl.gymcon;

import java.util.List;

public class TrainingPlan {
    private String planId;
    private String name;
    private List<TrainingStep> trainingSteps;

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TrainingStep> getTrainingSteps() {
        return trainingSteps;
    }

    public void setTrainingSteps(List<TrainingStep> trainingSteps) {
        this.trainingSteps = trainingSteps;
    }
}
