package gymcon.pl.gymcon;

public class Training {
    private TrainingPlan trainingPlan;
    private int step;

    public Training(TrainingPlan trainingPlan) {
        this.trainingPlan = trainingPlan;
        this.step = 1;
    }

    public String getNextMachineBeacon() {
        for (TrainingStep trainingStep : trainingPlan.getTrainingSteps()) {
            if (trainingStep.getOrder() == step) {
                return trainingStep.getMachine().getBeaconId();
            }
        }

        return "";
    }

    public boolean isFinished() {
        int maxOrder = 0;
        for (TrainingStep trainingStep : trainingPlan.getTrainingSteps()) {
            if (maxOrder < trainingStep.getOrder()) {
                maxOrder = trainingStep.getOrder();
            }
        }
        return maxOrder < step;
    }

    public void incrementStep() {
        step++;
    }

    public int getStep() {
        return step;
    }

    public String getPlanId() {
        return trainingPlan.getPlanId();
    }
}
