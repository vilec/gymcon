package gymcon.pl.gymcon;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class PlansActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.plans);

        new PlanListRequestService(this).execute("http://" + Constants.SERVER_IP + "/GymConServer/api/TrainingPlans/");
    }

    public void createPlans(List<TrainingPlan> trainingPlans) {
        final ListView listView = (ListView) findViewById(R.id.listView1);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TrainingPlan trainingPlan = (TrainingPlan) listView.getItemAtPosition(position);
                String planId = trainingPlan.getPlanId();
                Intent intent = new Intent(PlansActivity.this, MapActivity.class);
                intent.putExtra("planId", planId);
                startActivity(intent);
            }
        });
        listView.setAdapter(new PlanAdapter(this, R.layout.plan_item, trainingPlans));
    }
}
