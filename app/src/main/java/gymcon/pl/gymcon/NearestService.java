package gymcon.pl.gymcon;



import com.kontakt.sdk.android.common.profile.IBeaconDevice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class NearestService {

    public static final Set<String> OUR_BEACONS = new HashSet<>();
    private Set<BeaconInfo> beacons = new HashSet<>();


    public NearestService() {
        OUR_BEACONS.add("lpeK");
        OUR_BEACONS.add("iSF5");
        OUR_BEACONS.add("AmwF");
        OUR_BEACONS.add("SILW");
        OUR_BEACONS.add("1LMA");
        OUR_BEACONS.add("SZ2h");
        OUR_BEACONS.add("gfcg");
        OUR_BEACONS.add("QYre");
    }

    public BeaconInfo findNearest(List<IBeaconDevice> ibeacons){
        beacons.clear();;
        for (IBeaconDevice ibeaconDevice : ibeacons) {
            if (OUR_BEACONS.contains(ibeaconDevice.getUniqueId()) && ibeaconDevice.getDistance() < 0.2) {
                BeaconInfo beaconInfo = new BeaconInfo(ibeaconDevice);
                beacons.add(beaconInfo);
            }
        }

        if (beacons.size() > 0) {
            List<BeaconInfo> beaconInfos = new ArrayList<>(beacons);
            Collections.sort(beaconInfos, new Comparator<BeaconInfo>() {
                @Override
                public int compare(BeaconInfo lhs, BeaconInfo rhs) {
                    return Double.compare(lhs.getDistance(), rhs.getDistance());
                }
            });
            return beaconInfos.get(0);
        }
        return null;
    }
}
