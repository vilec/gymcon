package gymcon.pl.gymcon;

import android.os.AsyncTask;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class PlanListRequestService extends AsyncTask<String, Void, String> {
    private OkHttpClient client = new OkHttpClient();
    private PlansActivity activity;

    public PlanListRequestService(PlansActivity activity) {
        this.activity = activity;
    }

    @Override
    protected String doInBackground(String... params) {
        Request request = new Request.Builder()
                .url(params[0])
                .build();

        try {
            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    @Override
    protected void onPostExecute(String json) {
        List<TrainingPlan> trainingPlans = new TrainingPlanService().parse(json);
        activity.createPlans(trainingPlans);
    }
}
