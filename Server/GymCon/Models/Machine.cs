﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GymCon.Models
{
    [Table("Machine")]
    public class Machine
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MachineId { get; set; }

        [Display(Name = "Beacon Id")]
        public string BeaconId { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Image Url")]
        public string ImgUrl { get; set; }

        public bool InUse { get; set; }

        public int XCoord { get; set; }

        public int YCoord { get; set; }
    }

}