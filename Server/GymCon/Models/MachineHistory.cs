﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GymCon.Models
{
    [Table("MachineHistory")]
    public class MachineHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MachineHistoryId { get; set; }

        [ForeignKey("Machine")]
        public int MachineId { get; set; }

        public Machine Machine { get; set; }

        public string UserId { get; set; }

        public int WeightKg { get; set; }

        public int Repeats { get; set; }

        public DateTime TimeStamp { get; set; }
    }
}