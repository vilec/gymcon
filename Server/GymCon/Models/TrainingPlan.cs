﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GymCon.Models
{
    [Table("TrainingPlan")]
    public class TrainingPlan
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PlanId { get; set; }

        public string Name { get; set; }

        public virtual ICollection<TrainingStep> TrainingSteps { get; set; }
    }
}