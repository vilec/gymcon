using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GymCon.Models
{
    [Table("TrainingStep")]
    public class TrainingStep
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TrainingStepId { get; set; }

        [ForeignKey("TrainingPlan")]
        public int TrainingPlanId { get; set; }

        public TrainingPlan TrainingPlan { get; set; }

        [ForeignKey("Machine")]
        public int MachineId { get; set; }

        public Machine Machine { get; set; }

        public int Order { get; set; }

        public int WeightKg { get; set; }

        public int Repeats { get; set; }

        public int Series { get; set; }
    }
}