namespace GymCon.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingtimestamp : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MachineHistory", "TimeStamp", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MachineHistory", "TimeStamp");
        }
    }
}
