namespace GymCon.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Next : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TrainingStep", "WeightKg", c => c.Int(nullable: false));
            AddColumn("dbo.TrainingStep", "Repeats", c => c.Int(nullable: false));
            AddColumn("dbo.TrainingStep", "Series", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TrainingStep", "Series");
            DropColumn("dbo.TrainingStep", "Repeats");
            DropColumn("dbo.TrainingStep", "WeightKg");
        }
    }
}
