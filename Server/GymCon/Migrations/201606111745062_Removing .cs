namespace GymCon.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Removing : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MachineHistory", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.MachineHistory", new[] { "UserId" });
            AlterColumn("dbo.MachineHistory", "UserId", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MachineHistory", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.MachineHistory", "UserId");
            AddForeignKey("dbo.MachineHistory", "UserId", "dbo.AspNetUsers", "Id");
        }
    }
}
