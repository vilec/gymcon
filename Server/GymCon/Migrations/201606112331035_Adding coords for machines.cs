namespace GymCon.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingcoordsformachines : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Machine", "XCoord", c => c.Int(nullable: false));
            AddColumn("dbo.Machine", "YCoord", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Machine", "YCoord");
            DropColumn("dbo.Machine", "XCoord");
        }
    }
}
