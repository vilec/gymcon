﻿using System;
using System.Web.Http.Results;
using AutoMapper;
using GymCon.Models;
using GymCon.Transport;

namespace GymCon
{
    public static class AutomapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(Action);
        }

        private static void Action(IMapperConfiguration mapperConfiguration)
        {
            mapperConfiguration.CreateMap<Machine, MachineDto>();
            mapperConfiguration.CreateMap<TrainingStep, TrainingStepDto>();
            mapperConfiguration.CreateMap<TrainingPlan, TrainingPlanDto>();
            mapperConfiguration.CreateMap<MachineHistoryPutDto, MachineHistory>()
                .ForMember(history => history.Machine, o => o.Ignore())
                .ForMember(history => history.MachineHistoryId, o => o.Ignore());

        }
    }
}