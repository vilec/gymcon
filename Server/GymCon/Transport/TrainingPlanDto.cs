﻿using System.Collections.Generic;

namespace GymCon.Transport
{
    public class TrainingPlanDto
    {
        public int PlanId { get; set; }

        public string Name { get; set; }

        public List<TrainingStepDto> TrainingSteps { get; set; } 
    }
}