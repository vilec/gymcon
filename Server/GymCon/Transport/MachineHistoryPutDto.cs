﻿using System;

namespace GymCon.Transport
{
    public class MachineHistoryPutDto
    {
        public int UserId { get; set; }

        public int MachineId { get; set; }

        public int WeightKg { get; set; }

        public int Repeats { get; set; }
    }
}