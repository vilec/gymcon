﻿namespace GymCon.Transport
{
    public class MachineDto
    {
        public int MachineId { get; set; }

        public string BeaconId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ImgUrl { get; set; }

        public bool InUse { get; set; }

        public int XCoord { get; set; }

        public int YCoord { get; set; }
    }
}