﻿namespace GymCon.Transport
{
    public class TrainingStepDto
    {
        public MachineDto Machine { get; set; }

        public int Order { get; set; }

        public int WeightKg { get; set; }

        public int Repeats { get; set; }

        public int Series { get; set; }
    }
}