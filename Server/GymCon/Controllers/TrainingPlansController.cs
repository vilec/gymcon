﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using GymCon.Models;
using GymCon.Transport;

namespace GymCon.Controllers
{
    public class TrainingPlansController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/TrainingPlans
        public IEnumerable<TrainingPlanDto> GetTrainingPlans()
        {
            return db.TrainingPlans.Include("TrainingSteps.Machine").Select(Mapper.Map<TrainingPlanDto>);
        }

        // GET: api/TrainingPlans/5
        [ResponseType(typeof(TrainingPlan))]
        public IHttpActionResult GetTrainingPlan(int id)
        {
            TrainingPlan trainingPlan = db.TrainingPlans.Find(id);
            if (trainingPlan == null)
            {
                return NotFound();
            }

            return Ok(trainingPlan);
        }

        // PUT: api/TrainingPlans/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTrainingPlan(int id, TrainingPlan trainingPlan)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != trainingPlan.PlanId)
            {
                return BadRequest();
            }

            db.Entry(trainingPlan).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TrainingPlanExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TrainingPlans
        [ResponseType(typeof(TrainingPlan))]
        public IHttpActionResult PostTrainingPlan(TrainingPlan trainingPlan)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TrainingPlans.Add(trainingPlan);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = trainingPlan.PlanId }, trainingPlan);
        }

        // DELETE: api/TrainingPlans/5
        [ResponseType(typeof(TrainingPlan))]
        public IHttpActionResult DeleteTrainingPlan(int id)
        {
            TrainingPlan trainingPlan = db.TrainingPlans.Find(id);
            if (trainingPlan == null)
            {
                return NotFound();
            }

            db.TrainingPlans.Remove(trainingPlan);
            db.SaveChanges();

            return Ok(trainingPlan);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TrainingPlanExists(int id)
        {
            return db.TrainingPlans.Count(e => e.PlanId == id) > 0;
        }
    }
}