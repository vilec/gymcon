﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GymCon.Models;

namespace GymCon.Controllers
{
    public class TrainingStepController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TrainingSteps1
        public ActionResult Index()
        {
            var trainingSteps = db.TrainingSteps.Include(t => t.Machine).Include(t => t.TrainingPlan);
            return View(trainingSteps.ToList());
        }

        // GET: TrainingSteps1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingStep trainingStep = db.TrainingSteps.Find(id);
            if (trainingStep == null)
            {
                return HttpNotFound();
            }
            return View(trainingStep);
        }

        // GET: TrainingSteps1/Create
        public ActionResult Create()
        {
            ViewBag.MachineId = new SelectList(db.Machines, "MachineId", "BeaconId");
            ViewBag.TrainingPlanId = new SelectList(db.TrainingPlans, "PlanId", "Name");
            return View();
        }

        // POST: TrainingSteps1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TrainingStepId,TrainingPlanId,MachineId,Order,WeightKg,Repeats,Series")] TrainingStep trainingStep)
        {
            if (ModelState.IsValid)
            {
                db.TrainingSteps.Add(trainingStep);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MachineId = new SelectList(db.Machines, "MachineId", "BeaconId", trainingStep.MachineId);
            ViewBag.TrainingPlanId = new SelectList(db.TrainingPlans, "PlanId", "Name", trainingStep.TrainingPlanId);
            return View(trainingStep);
        }

        // GET: TrainingSteps1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingStep trainingStep = db.TrainingSteps.Find(id);
            if (trainingStep == null)
            {
                return HttpNotFound();
            }
            ViewBag.MachineId = new SelectList(db.Machines, "MachineId", "BeaconId", trainingStep.MachineId);
            ViewBag.TrainingPlanId = new SelectList(db.TrainingPlans, "PlanId", "Name", trainingStep.TrainingPlanId);
            return View(trainingStep);
        }

        // POST: TrainingSteps1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TrainingStepId,TrainingPlanId,MachineId,Order,WeightKg,Repeats,Series")] TrainingStep trainingStep)
        {
            if (ModelState.IsValid)
            {
                db.Entry(trainingStep).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MachineId = new SelectList(db.Machines, "MachineId", "BeaconId", trainingStep.MachineId);
            ViewBag.TrainingPlanId = new SelectList(db.TrainingPlans, "PlanId", "Name", trainingStep.TrainingPlanId);
            return View(trainingStep);
        }

        // GET: TrainingSteps1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingStep trainingStep = db.TrainingSteps.Find(id);
            if (trainingStep == null)
            {
                return HttpNotFound();
            }
            return View(trainingStep);
        }

        // POST: TrainingSteps1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TrainingStep trainingStep = db.TrainingSteps.Find(id);
            db.TrainingSteps.Remove(trainingStep);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
