﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using GymCon.Models;
using GymCon.Transport;

namespace GymCon.Controllers
{
    public class MachineHistoriesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/MachineHistories/5
        [ResponseType(typeof(IEnumerable<MachineHistory>))]
        public IHttpActionResult GetMachineHistory(int userId, int machineId)
        {
            var machineHistories =
                db.MachineHistories.Where(
                    history => history.MachineId == machineId && history.UserId == userId.ToString())
                    .OrderByDescending(history => history.TimeStamp)
                    .FirstOrDefault();


            return Ok(machineHistories);
        }

        // PUT: api/MachineHistories/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMachineHistory(MachineHistoryPutDto machineHistoryDto)
        {
            MachineHistory machineHistory = Mapper.Map<MachineHistory>(machineHistoryDto);
            machineHistory.TimeStamp = DateTime.UtcNow;
            db.MachineHistories.Add(machineHistory);
                db.SaveChanges();


            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MachineHistories
        [ResponseType(typeof(MachineHistory))]
        public IHttpActionResult PostMachineHistory(MachineHistory machineHistory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MachineHistories.Add(machineHistory);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = machineHistory.MachineHistoryId }, machineHistory);
        }

        // DELETE: api/MachineHistories/5
        [ResponseType(typeof(MachineHistory))]
        public IHttpActionResult DeleteMachineHistory(int id)
        {
            MachineHistory machineHistory = db.MachineHistories.Find(id);
            if (machineHistory == null)
            {
                return NotFound();
            }

            db.MachineHistories.Remove(machineHistory);
            db.SaveChanges();

            return Ok(machineHistory);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MachineHistoryExists(int id)
        {
            return db.MachineHistories.Count(e => e.MachineHistoryId == id) > 0;
        }
    }
}