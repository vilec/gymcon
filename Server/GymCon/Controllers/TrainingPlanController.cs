﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GymCon.Models;

namespace GymCon.Controllers
{
    public class TrainingPlanController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TrainingPlans1
        public ActionResult Index()
        {
            return View(db.TrainingPlans.ToList());
        }

        // GET: TrainingPlans1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingPlan trainingPlan = db.TrainingPlans.Find(id);
            if (trainingPlan == null)
            {
                return HttpNotFound();
            }
            return View(trainingPlan);
        }

        // GET: TrainingPlans1/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TrainingPlans1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PlanId,Name")] TrainingPlan trainingPlan)
        {
            if (ModelState.IsValid)
            {
                db.TrainingPlans.Add(trainingPlan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(trainingPlan);
        }

        // GET: TrainingPlans1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingPlan trainingPlan = db.TrainingPlans.Find(id);
            if (trainingPlan == null)
            {
                return HttpNotFound();
            }
            return View(trainingPlan);
        }

        // POST: TrainingPlans1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PlanId,Name")] TrainingPlan trainingPlan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(trainingPlan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(trainingPlan);
        }

        // GET: TrainingPlans1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingPlan trainingPlan = db.TrainingPlans.Find(id);
            if (trainingPlan == null)
            {
                return HttpNotFound();
            }
            return View(trainingPlan);
        }

        // POST: TrainingPlans1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TrainingPlan trainingPlan = db.TrainingPlans.Find(id);
            db.TrainingPlans.Remove(trainingPlan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
