﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using GymCon.Models;

namespace GymCon.Controllers
{
    public class TrainingStepsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/TrainingSteps1
        public IQueryable<TrainingStep> GetTrainingSteps()
        {
            return db.TrainingSteps;
        }

        // GET: api/TrainingSteps1/5
        [ResponseType(typeof(TrainingStep))]
        public IHttpActionResult GetTrainingStep(int id)
        {
            TrainingStep trainingStep = db.TrainingSteps.Find(id);
            if (trainingStep == null)
            {
                return NotFound();
            }

            return Ok(trainingStep);
        }

        // PUT: api/TrainingSteps1/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTrainingStep(int id, TrainingStep trainingStep)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != trainingStep.TrainingStepId)
            {
                return BadRequest();
            }

            db.Entry(trainingStep).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TrainingStepExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TrainingSteps1
        [ResponseType(typeof(TrainingStep))]
        public IHttpActionResult PostTrainingStep(TrainingStep trainingStep)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TrainingSteps.Add(trainingStep);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = trainingStep.TrainingStepId }, trainingStep);
        }

        // DELETE: api/TrainingSteps1/5
        [ResponseType(typeof(TrainingStep))]
        public IHttpActionResult DeleteTrainingStep(int id)
        {
            TrainingStep trainingStep = db.TrainingSteps.Find(id);
            if (trainingStep == null)
            {
                return NotFound();
            }

            db.TrainingSteps.Remove(trainingStep);
            db.SaveChanges();

            return Ok(trainingStep);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TrainingStepExists(int id)
        {
            return db.TrainingSteps.Count(e => e.TrainingStepId == id) > 0;
        }
    }
}